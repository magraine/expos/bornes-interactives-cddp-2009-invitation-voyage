;(function($) {	
$(document).ready(function(){
	$(".partie_de_carte").hide();
	$("body.page_planche").addClass("curseur_on");
	
	var zonesVisibles = false;
	$(document).keypress(function(e){
		// control + alt + e : fenetre de selection de zone de survol
		if ((e.charCode == 101) && (e.ctrlKey == true) && (e.metaKey == true)) {
			if ($("body").is(".coordonnees_on")) {
				$("body").addClass("curseur_on").removeClass("coordonnees_on");
				$(".image_map img").imgAreaSelect({hide: true, disable: true});
			} else {
				$("body").removeClass("curseur_on").addClass("coordonnees_on");
				$(".image_map img").imgAreaSelect({enable: true, show: true, x1:0,x2:0,y1:0,y2:0, onSelectChange: afficherCoordonnees });
				// cacher les trucs ouverts :
				$(".partie_de_carte").removeClass("conserver").hide();
			}
		}
		// control + alt + i : afficher les zones
		if ((e.charCode == 105) && (e.ctrlKey == true) && (e.metaKey == true)) {
			if (zonesVisibles == false) {
				zonesVisibles = true;
			} else {
				zonesVisibles = false;
			}

			$(".image_map img").maphilight({
				fill: zonesVisibles,
				fillColor: "ffffff",
				fillOpacity: 0.2,
				stroke: zonesVisibles,
				strokeColor: "ff3300",
				strokeOpacity: 1,
				strokeWidth: 1,
				fade: false,
				alwaysOn: zonesVisibles,
				enable: zonesVisibles
			});
		}
		
		// control + alt + z : garder ouvert une zone que l on survole
		if ((e.charCode == 122) && (e.ctrlKey == true) && (e.metaKey == true)) {
			if (partieVisible>0) {
				garderPartie(partieVisible);
			}
		}
		
		// espace : lire un texte
		if ((e.charCode == 32) && (e.metaKey == false) && (e.shiftKey == false)) {
			if (partieVisible>0) {
				var son_texte = $("#partie_de_carte"+partieVisible+" .son_texte a").attr("href");
				if (son_texte) {
					/*soundManager.stop("son"+partieVisible);*/
					soundManager.sounds["son"+partieVisible].setVolume(10);
					soundManager.play("son_texte"+partieVisible,son_texte);
				}
			}
		}					
	});

	// lorsque les selects (en mode edition de selection) changent,
	// il faut rafraichir le texte
	$("#zoneArticle select").change(function(){afficherTexteModele();});
	
	var x1=0,y1=0,x2=0,y2=0;
	nb = $(".modele_img_carte area").size();
	$("#zoneArticle select[name=nb] option[value="+ ++nb +"]").attr("selected","selected");
	
	function afficherTexteModele(){
		var nb = $("#zoneArticle select[name=nb]").val();
		var titre = $("#zoneArticle select[name=id_article] option:selected").attr("title");
		var id_article = $("#zoneArticle select[name=id_article]").val();
		var alignement = $("#zoneArticle select[name=alignement]").val();
		var texte = 
			"|" + titre +
			"<br />|type" + nb + "=rect" +
			"<br />|coord" + nb + "=" + x1 + "," + y1 + "," + x2 + "," + y2 +
			"<br />|lien" + nb + "=" + id_article +
			"<br />|infos" + nb + "=" + alignement;	
		$("#coordonnees .affichage").html(texte);				
	}
	
	function afficherCoordonnees(img, s){
		x1 = s.x1; y1 = s.y1;
		x2 = s.x2; y2 = s.y2;
		afficherTexteModele();
	}
});
})(jQuery);


// connaitre la partie qui s ouvre
var partieVisible = 0;
function voirPartie(nb){
	soundManager.stopAll();
	
	partieVisible = nb;
	jQuery("#partie_de_carte"+nb).show();
	var son = jQuery("#partie_de_carte"+nb+" .son a").attr("href");
	if (son) {
		soundManager.play("son"+nb,son);
	}
}

function cacherPartie(nb){
	partieVisible = 0;
	jQuery("#partie_de_carte"+nb+":not(.conserver)").hide();
	soundManager.stop("son"+nb);
}


// forcer une partie ouverte a etre affichee
var partieOuverte = 0;
function garderPartie(nb){
	var po = partieOuverte;
	if (po != nb) {
		jQuery("#partie_de_carte"+nb).addClass("conserver");
		jQuery("body").removeClass("curseur_on");	
		jQuery("#partie_de_carte"+po).hide();		
		partieOuverte = nb;
	} else {
		partieOuverte = 0;
		jQuery("body").addClass("curseur_on");
	}
	// fermer l ancien (ou l actuel si second double click)
	jQuery("#partie_de_carte"+po).removeClass("conserver");
	
}
